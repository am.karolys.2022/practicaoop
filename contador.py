class Cuenta:
    def __init__(self, nom, num, sal=0):
        self.titular = nom
        self.numero = num
        self.saldo = sal

    def ingreso(self, cantidad):
        self.saldo = self.saldo + cantidad

    def reintegro(self,  cantidad):
        self.saldo = self.saldo - cantidad


    def transferenciaM(self, dst, cantidad):  # metodo(cuando le da las orden)  # funcion, llamas a la funcion que hace algo, no recibe una orden
        self.reintegro(cantidad)
        dst.ingreso(cantidad)

    def transferenciaF(org, dst, cantidad):  # metodo(cuando le da las orden)  # funcion, llamas a la funcion que hace algo, no recibe una orden
        org.reintegro(cantidad)
        dst.ingreso(cantidad)

    def __str__(self):
        return "#{num} con saldo: {sal}".format(num=self.numero, sal=self.saldo)



cc1 = Cuenta("MarinaN", 1)
cc2 = Cuenta("MarinaI", 2, 5000)


print(cc1)
print(cc2)
cc1.ingreso(3000)
cc2.ingreso(3000)

# metodo
cc2.transferenciaM(cc1, 100)
print(cc1)
print(cc2)

# funcion
Cuenta.transferenciaF(cc1, cc2, 200)
print(cc1)
print(cc2)